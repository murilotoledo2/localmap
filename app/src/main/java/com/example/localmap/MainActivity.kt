package com.example.localmap

import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.preference.PreferenceManager
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import org.osmdroid.bonuspack.routing.OSRMRoadManager
import org.osmdroid.bonuspack.routing.Road
import org.osmdroid.bonuspack.routing.RoadManager
import org.osmdroid.config.Configuration.getInstance
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Polyline


class MainActivity : AppCompatActivity() {

    //OSMDROID AQUI======================================================================

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 1
    private lateinit var map : MapView

    lateinit var texto: TextView
    lateinit var rotaInicio: Spinner
    lateinit var rotaFim: Spinner
    lateinit var botao: Button
    var latitudeS: Double = 0.0
    var longitudeS: Double = 0.0
    var latitudeE: Double = 0.0
    var longitudeE: Double = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //dropList (spinner)

        rotaInicio = findViewById(R.id.spinner)
        rotaFim = findViewById(R.id.spinner2)
        botao = findViewById(R.id.btn_ir)

        var locais = arrayOf("IFTM - Unidade 1", "IFTM - Unidade 2", "IFTM - Campus Uberaba")
        //spinner.adapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item, locais)
        rotaInicio.adapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item, locais)
        rotaFim.adapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item, locais)

        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        //MENUS DO APP
        /*
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        binding.appBarMain.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        */

        //INICIO DO ONCREATE DO OSMDROID

        //handle permissions first, before map is created. not depicted here

        //load/initialize the osmdroid configuration, this can be done
        // This won't work unless you have imported this: org.osmdroid.config.Configuration.*
        getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this))
        //setting this before the layout is inflated is a good idea
        //it 'should' ensure that the map has a writable location for the map cache, even without permissions
        //if no tiles are displayed, you can try overriding the cache path using Configuration.getInstance().setCachePath
        //see also StorageUtils
        //note, the load method also sets the HTTP User Agent to your application's package name, if you abuse osm's
        //tile servers will get you banned based on this string.

        //inflate and create the map
        map = findViewById<MapView>(R.id.map)
        map.setTileSource(TileSourceFactory.MAPNIK)

        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);

        //centralizando o mapa em um ponto de interesse
        val mapController = map.controller
        mapController.setZoom(17.0)
        //IFTM PARQUE TECNOLOGICO
        val startPoint = GeoPoint(-19.717201424855705, -47.95773937044628);
        mapController.setCenter(startPoint);

        val startMarker = Marker(map)
        startMarker.position = startPoint
        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        map.overlays.add(startMarker)

        map.invalidate();

        //tentativa de mudar icone do marcador
        //startMarker.setIcon(getResources().getDrawable(R.drawable.marker_default_focused_base));
        //startMarker.setIcon(getResources().getDrawable(R.drawable.ic_launcher));

        //descrição do marcador
        startMarker.setTitle("IFTM - Parque Tecnológico");

        //INICIANDO A ROTA
        val roadManager: RoadManager = OSRMRoadManager(this, USER_SERVICE)

        val waypoints = ArrayList<GeoPoint>()
        waypoints.add(startPoint)
        val endPoint = GeoPoint(-19.770248223588037, -47.94845761114034)
        waypoints.add(endPoint)

        //criando um ponto para o iftm abcz
        val iftmABCZ = Marker(map)
        iftmABCZ.position = endPoint
        map.overlays.add(iftmABCZ)
        iftmABCZ.setTitle("IFTM - (Proximo ao ABCZ)")

        val road: Road = roadManager.getRoad(waypoints)

        val roadOverlay: Polyline = RoadManager.buildRoadOverlay(road)

        map.getOverlays().add(roadOverlay);

        map.invalidate();

        //COLOCANDO PASSOS NA ROTA com INSTRUÇÕES

        val nodeIcon = resources.getDrawable(R.drawable.ic_direcao)
        for (i in road.mNodes.indices) {
            val node = road.mNodes[i]
            val nodeMarker = Marker(map)
            nodeMarker.position = node.mLocation
            nodeMarker.icon = nodeIcon
            nodeMarker.title = "Step $i"
            //descricao do no
            nodeMarker.setSnippet(node.mInstructions)
            nodeMarker.setSubDescription(Road.getLengthDurationText(this,node.mLength,node.mDuration))
            val icon = resources.getDrawable(org.osmdroid.bonuspack.R.drawable.moreinfo_arrow)
            nodeMarker.setImage(icon);

            map.overlays.add(nodeMarker)
        }

    }

    fun onClickIr(v: View){


        //centralizando o mapa em um ponto de interesse
        val mapController = map.controller
        mapController.setZoom(17.0)

        map.overlays.clear()
        map.invalidate()

        val localInicio = rotaInicio.selectedItem.toString()
        val localFim = rotaFim.selectedItem.toString()


        //if do start
        if(localInicio.equals("IFTM - Unidade 1"))
        {
            latitudeS = -19.717201424855705
            longitudeS = -47.95773937044628
        }else{
            if(localInicio.equals("IFTM - Unidade 2")){
                latitudeS = -19.770248223588037
                longitudeS = -47.94845761114034
            }else{
                if(localInicio.equals("IFTM - Campus Uberaba")){
                    latitudeS = -19.665755407875103
                    longitudeS =-47.96985440271129
                }
            }
        }

        //if do final
        if(localFim.equals("IFTM - Unidade 1"))
        {
            latitudeE = -19.717201424855705
            longitudeE = -47.95773937044628
        }else{
            if(localFim.equals("IFTM - Unidade 2")){
                latitudeE = -19.770248223588037
                longitudeE = -47.94845761114034
            }else{
                if(localFim.equals("IFTM - Campus Uberaba")){
                    latitudeE = -19.665755407875103
                    longitudeE =-47.96985440271129
                }
            }
        }



        val startPoint = GeoPoint(latitudeS,longitudeS)
        mapController.setCenter(startPoint);

        val startMarker = Marker(map)
        startMarker.position = startPoint
        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        map.overlays.add(startMarker)

        map.invalidate();

        //descrição do marcador
        startMarker.setTitle("IFTM - Parque Tecnológico");

        //INICIANDO A ROTA
        val roadManager: RoadManager = OSRMRoadManager(this, USER_SERVICE)

        val waypoints = ArrayList<GeoPoint>()
        waypoints.add(startPoint)
        val endPoint = GeoPoint(latitudeE,longitudeE)
        waypoints.add(endPoint)

        //criando um ponto para o iftm abcz
        val iftmABCZ = Marker(map)
        iftmABCZ.position = endPoint
        map.overlays.add(iftmABCZ)
        iftmABCZ.setTitle("IFTM - (Proximo ao ABCZ)")

        val road: Road = roadManager.getRoad(waypoints)

        val roadOverlay: Polyline = RoadManager.buildRoadOverlay(road)

        map.getOverlays().add(roadOverlay);

        map.invalidate();

        //COLOCANDO PASSOS NA ROTA com INSTRUÇÕES

        val nodeIcon = resources.getDrawable(R.drawable.ic_direcao)
        for (i in road.mNodes.indices) {
            val node = road.mNodes[i]
            val nodeMarker = Marker(map)
            nodeMarker.position = node.mLocation
            nodeMarker.icon = nodeIcon
            nodeMarker.title = "Step $i"
            //descricao do no
            nodeMarker.setSnippet(node.mInstructions)
            nodeMarker.setSubDescription(Road.getLengthDurationText(this,node.mLength,node.mDuration))
            val icon = resources.getDrawable(org.osmdroid.bonuspack.R.drawable.moreinfo_arrow)
            nodeMarker.setImage(icon);

            map.overlays.add(nodeMarker)
        }

    }

    override fun onResume() {
        super.onResume()
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume() //needed for compass, my location overlays, v6.0.0 and up
    }

    override fun onPause() {
        super.onPause()
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        map.onPause()  //needed for compass, my location overlays, v6.0.0 and up
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        val permissionsToRequest = ArrayList<String>()
        var i = 0
        while (i < grantResults.size) {
            permissionsToRequest.add(permissions[i])
            i++
        }
        if (permissionsToRequest.size > 0) {
            ActivityCompat.requestPermissions(
                this,
                permissionsToRequest.toTypedArray(),
                REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }


    /*private fun requestPermissionsIfNecessary(String[] permissions) {
        val permissionsToRequest = ArrayList<String>();
        permissions.forEach { permission ->
        if (ContextCompat.checkSelfPermission(this, permission)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            permissionsToRequest.add(permission);
        }
    }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(
                    this,
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }*/

    //FIM OSMDROID=======================================================================

    //MENU LATERAL PARA ADICIONAR FUTURAMENTE============================================
    /*
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    /*
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        binding.appBarMain.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }*/


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }



    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
    */

}